#include "Brick.h"
#include "Maths.h"
#include "InputState.h"
#include "InputMemoryBitStream.h"
#include "OutputMemoryBitStream.h"

//zoom hardcoded at 100...if we want to lock players on screen, this could be calculated from zoom
const float HALF_WORLD_HEIGHT = 3.6f;
const float HALF_WORLD_WIDTH = 6.4f;

Brick::Brick() :GameObject() ,mBrickId(0)
{

}

void Brick::Update()
{

}

/// This is the Brick Write for sending brick data information packets into bits
uint32_t Brick::Write( OutputMemoryBitStream& inOutputStream, uint32_t inDirtyState ) const
{
	/// Sends and writes BrickId, replicated location into memory packets
	inOutputStream.Write(GetBrickId());

	Vector3 location = GetLocation();
	inOutputStream.Write(location.mX);
	inOutputStream.Write(location.mY);

	/*inOutputStream.Write(GetRotation());
	inOutputStream.Write(GetColor());*/
	return(0);
}

bool Brick::operator==(Brick &other)
{
	// Game Object Part.
	//Call the == of the base, Player reference is
	//downcast explicitly.
	if (!GameObject::operator==(other)) return false;

	if (this->ECRS_AllState != other.ECRS_AllState) return false;

	if (this->mBrickId != other.mBrickId) return false;

	return true;
}
