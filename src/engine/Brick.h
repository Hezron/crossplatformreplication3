#ifndef BRICK_H_
#define BRICK_H_

#include "GameObject.h"
#include "World.h"

class InputState;

class Brick : public GameObject
{
public:
	CLASS_IDENTIFICATION('BRCK', GameObject)

	enum EBrickReplicationState
	{
		ECRS_BrickId = 1 << 0,
		ECRS_Pose = 1 << 1,

		ECRS_AllState = ECRS_BrickId | ECRS_Pose
	};


	static	GameObject*	StaticCreate() { return new Brick(); }

	//Note - the code in the book doesn't provide this until the client.
	//This however limits testing.
	static	GameObjectPtr	StaticCreatePtr() { return GameObjectPtr(new Brick()); }


	virtual uint32_t GetAllStateMask()	const override { return ECRS_AllState; }

	virtual void Update() override;

	void		SetBrickId(uint32_t inBrickId) { mBrickId = inBrickId; }
	uint32_t	GetBrickId()						const { return mBrickId; }


	//	virtual void	Read( InputMemoryBitStream& inInputStream ) override;

	uint32_t Write( OutputMemoryBitStream& inOutputStream, uint32_t inDirtyState ) const;


	bool operator==(Brick &other);
protected:
	Brick();

private:
	uint32_t			mBrickId;


};

typedef shared_ptr< Brick >	BrickPtr;

#endif // BRICK_H_