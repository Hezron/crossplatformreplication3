#include "BrickClient.h"

#include "TextureManager.h"
#include "GameObjectRegistry.h"

#include "InputMemoryBitStream.h"
#include "OutputMemoryBitStream.h"
#include "NetworkManagerClient.h"

#include "StringUtils.h"

BrickClient::BrickClient() //: mTimeLocationBecameOutOfSync(0.f)
{
	mSpriteComponent.reset(new SpriteComponent(this));
	mSpriteComponent->SetTexture(TextureManager::sInstance->GetTexture("brick"));
	this->SetScale(0.1f);
}

void BrickClient::Update()
{
	//for now, we don't simulate any movement on the client side
	//we only move when the server tells us to move
}

/// This is the BrickClient Read for receiving memory bits from Write
void BrickClient::Read(InputMemoryBitStream& inInputStream)
{

	/// Receives and reads BrickId, replicated location memory information
	uint32_t brickId;
	inInputStream.Read(brickId);
	SetBrickId(brickId);

	float oldRotation = GetRotation();
	Vector3 oldLocation = GetLocation();
	Vector3 replicatedLocation;
	inInputStream.Read(replicatedLocation.mX);
	inInputStream.Read(replicatedLocation.mY);
	SetLocation(replicatedLocation);

	/*Vector3 color;
	inInputStream.Read(color);
	SetColor(color);*/
}




