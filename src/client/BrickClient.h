#pragma once
#ifndef BRICK_CLIENT_H
#define BRICK_CLIENT_H

#include "Brick.h"
#include "SpriteComponent.h"

class BrickClient : public Brick
{
public:
	static	GameObjectPtr	StaticCreate() { return GameObjectPtr(new BrickClient()); }

	virtual void Update();

	virtual void	Read(InputMemoryBitStream& inInputStream) override;

protected:
	BrickClient();


private:
	//float				mTimeLocationBecameOutOfSync;

	SpriteComponentPtr	mSpriteComponent;
};
typedef shared_ptr< BrickClient >	BrickClientPtr;
#endif //BRICK_CLIENT_H