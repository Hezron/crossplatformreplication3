#pragma once
#ifndef BRICK_SERVER_H
#define BRICK_SERVER_H

#include "Brick.h"
#include "NetworkManagerServer.h"

class BrickServer : public Brick
{
public:
	static GameObjectPtr	StaticCreate() { return NetworkManagerServer::sInstance->RegisterAndReturn(new BrickServer()); }

	virtual void Update() override;

protected:
	BrickServer();

private:


};

#endif // BRICK_SERVER_H